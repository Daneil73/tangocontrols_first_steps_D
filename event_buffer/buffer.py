import numpy as np
import taurus_pyqtgraph as tqt
import warnings
        
class Buffer:
    def __init__(self, size, bf_sz=None):
        """
        creates a buffer with the size that we have passed to it, like a queue structure
        """
        self.bf_sz = bf_sz
        self.buffer = []
        self.size = size

    def append(self, value):
        self.buffer.append(value)
        self.buffer = self.buffer[-self.size:]
        return self.buffer
        
    def getBuffer(self):
        return self.buffer
    
    def __len__(self):
        return len(self.buffer)
