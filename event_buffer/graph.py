from PyQt5 import QtCore, QtWidgets
import pyqtgraph as tqt
import numpy as np

class Widget(tqt.GraphicsLayoutWidget):
    def __init__(self, parent=None, buffer=None, device=None, atr=None, period=1000):
        super().__init__(parent=parent)

        self.buffer = buffer
        self.period = period
        self.mainLayout = QtWidgets.QDockWidget()
        
        self.timer = QtCore.QTimer(self)
        self.timer.setInterval(self.period) # in milliseconds
        self.timer.start()
        self.timer.timeout.connect(self.newData)
        
        self.qm = QtWidgets.QMainWindow()
        self.qm.addDockWidget(QtCore.Qt.DockWidgetArea.TopDockWidgetArea, self.mainLayout)
        self.plotItem = self.addPlot(title="{0}  -  {1}".format(device, atr))
        self.plotDataItem = self.plotItem.plot()

    def setData(self, x, y):
        self.plotDataItem.setData(x, y)

    def newData(self):
        value = self.buffer.getBuffer()
        clock = np.arange(len(self.buffer))
        self.setData(clock, value)
   