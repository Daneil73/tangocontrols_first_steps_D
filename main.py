import sys

from event_buffer.receiver import EventReceiver
from event_buffer.buffer import Buffer
from event_buffer.graph import *

def main():
    if len(sys.argv) < 4:
        print("insufficient attributes, script need 3 (device name, attribute, buffer size)")
        return

    # Passing vía command line
    device = sys.argv[1]
    atr = sys.argv[2]
    bf = int(sys.argv[3])

    # Create an object buffer and an event object
    buffer = Buffer(bf)
    event = EventReceiver(listener=buffer.append)

    # Async subscribe to the attribute device
    event.connect(device, atr)

    # Start widget (pyqtgraph)
    app = QtWidgets.QApplication([])
    win = Widget(buffer=buffer,parent=None, device=device, atr=atr)
    win.show()
    win.raise_()
    app.exec_()

if __name__ == '__main__':
    main()
